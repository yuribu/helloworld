
#define STB_IMAGE_IMPLEMENTATION

#include "common/includes.h"

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>


#include "vulkan/VulkanRenderer.h"

GLFWwindow* initWindow(std::string wName = "Test Window", const int width = 800, const int height = 600)
{
    // Initialise GLFW
    glfwInit();

    // Set GLFW to NOT work with OpenGL
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

    return glfwCreateWindow(width, height, wName.c_str(), nullptr, nullptr);
}

int main()
{
    // Create Window
    GLFWwindow* window = initWindow("Test Window", 1280, 720);

    // Create Vulkan Renderer instance
    try 
    {
        auto vulkanRenderer = std::make_unique<VulkanRenderer>(window);

        float angle = 0.0f;
        float deltaTime = 0.0f;
        float lastTime = 0.0f;

        vulkanRenderer->createMeshModel("Models/Black Hawk uh-60.dae");

        // Loop until closed
        while (!glfwWindowShouldClose(window))
        {
            glfwPollEvents();

            float now = glfwGetTime();
            deltaTime = now - lastTime;
            lastTime = now;

            angle += 50 * deltaTime;
            if (angle > 360.0f) { angle -= 360.0f; }

            /*
            glm::mat4 testMat = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0.0f, 1.0f, 0.0f));
            testMat = glm::rotate(testMat, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
            vulkanRenderer->updateModel(testMat);
            */

            vulkanRenderer->setModelsTransform(glm::rotate(glm::mat4(1.0), glm::radians(angle), glm::vec3(0.0f, 1.0f, 0.0f)));

            vulkanRenderer->draw();
        }

    }
    catch (const std::runtime_error& e)
    {
        printf("ERROR: %s\n", e.what());
        return EXIT_FAILURE;
    }

    // Destroy GLFW window and stop GLFW
    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}
