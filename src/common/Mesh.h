#pragma once

#include "Utilities.h"

struct Model {
    glm::mat4 model;
};

class Mesh
{
public:
    Mesh(size_t reserveVertices = 0, size_t reserveIndices = 0);
    Mesh(const std::vector<Vertex>& vertices, const std::vector<uint32_t>& indices);

    // parse dae
    size_t addVertex(const Vertex& vetrex);
    void addIndex(uint32_t index);

    // find existing vertex index, -1 on error
    size_t findVetrexIndex(const Vertex& vertex);

    void setModel(glm::mat4 newModel);
    Model getModel();

    void setMaterial(const std::string& newMaterial);
    std::string getMaterial();

    // load/rendering
    void loadMesh(VkPhysicalDevice newPhysicalDevice, VkDevice newDevice,
        VkQueue transferQueue, VkCommandPool transferCommandPool, int newTexId);

    int getTexId();

    int getVertexCount();
    VkBuffer getVertexBuffer();

    int getIndexCount();
    VkBuffer getIndexBuffer();

    void destroyBuffers();

private:
    Model model;
    std::string material;
    int texId;

    std::vector<Vertex> vertices; 
    std::vector<uint32_t> indices;

    int vertexCount;
    VkBuffer vertexBuffer;
    VkDeviceMemory vertexBufferMemory;

    int indexCount;
    VkBuffer indexBuffer;
    VkDeviceMemory indexBufferMemory;

    VkPhysicalDevice physicalDevice;
    VkDevice device;

    void createVertexBuffer(VkQueue transferQueue, VkCommandPool transferCommandPool);
    void createIndexBuffer(VkQueue transferQueue, VkCommandPool transferCommandPool);
};

