#pragma once

#include <stdexcept>
#include <string>
#include <vector>
#include <memory>
#include <array>
#include <tuple>
#include <map>
#include <set>
#include <sstream>
#include <iostream>

#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/ext/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale
#include <glm/ext/matrix_clip_space.hpp> // glm::perspective
#include <glm/ext/scalar_constants.hpp> // glm::pi

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#define THROW(message) throw std::runtime_error(std::string(__FILE__) + ":" + std::to_string(__LINE__) + ": " + message);
#define THROW_IF(condition, message) if (condition) THROW(message)