#pragma once

#include "Mesh.h"

class ModelTree
{
public:
    ModelTree();

    void addMesh(const Mesh& newMeshList);

    void addChild(const ModelTree& modelTree);

    void loadTree(VkPhysicalDevice newPhysicalDevice, VkDevice newDevice,
        VkQueue transferQueue, VkCommandPool transferCommandPool, 
        std::map<std::string, int>& textureIndices);

    void drawTree(VkCommandBuffer commandBuffer, VkPipelineLayout pipelineLayout, 
        VkDescriptorSet descriptorSet, const std::vector<VkDescriptorSet>& samplerDescriptors);

    size_t getMeshCount();
    Mesh* getMesh(size_t index);

    glm::mat4 getModel();
    void setTransform(const glm::mat4& tf);
    glm::mat4 getTransform() { return transform; }

    void applyTransform(const glm::mat4& transform);

    void rotate(float angle, const glm::vec3& axis);

    void destroyMeshModel();

private:
    std::vector<Mesh> meshList;
    glm::mat4 model;
    glm::mat4 transform;

    std::vector<ModelTree> children;
};