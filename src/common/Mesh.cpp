
#include "includes.h" // precompiled header first

#include "Mesh.h"

using namespace std;

Mesh::Mesh(size_t reserveVertices, size_t reserveIndices)
{
    vertices.reserve(reserveVertices);
    indices.reserve(reserveIndices);
}

Mesh::Mesh(const vector<Vertex>& vertices, const vector<uint32_t>& indices)
    :vertices(vertices), indices(indices)
{
    model.model = glm::mat4(1.0f);
}

size_t Mesh::addVertex(const Vertex& vetrex)
{
    vertices.push_back(vetrex);
    return vertices.size();
}

void Mesh::addIndex(uint32_t index)
{
    indices.push_back(index);
}

size_t Mesh::findVetrexIndex(const Vertex& vertex)
{
    auto findIt = find(vertices.begin(), vertices.end(), vertex);
    if (findIt == vertices.end())
    {
        return -1;
    }

    return findIt - vertices.begin();
}

void Mesh::setModel(glm::mat4 newModel)
{
    model.model = newModel;
}

Model Mesh::getModel()
{
    return model;
}

void Mesh::setMaterial(const std::string& newMaterial)
{
    material = newMaterial;
}

std::string Mesh::getMaterial()
{
    return material;
}

void Mesh::loadMesh(VkPhysicalDevice newPhysicalDevice, VkDevice newDevice,
    VkQueue transferQueue, VkCommandPool transferCommandPool, int newTexId)
{
    vertexCount = vertices.size();
    indexCount = indices.size();
    physicalDevice = newPhysicalDevice;
    device = newDevice;
    texId = newTexId;

    createVertexBuffer(transferQueue, transferCommandPool);
    createIndexBuffer(transferQueue, transferCommandPool);

    // not needed
    vertices.clear();
    indices.clear();
}

int Mesh::getTexId()
{
    return texId;
}

int Mesh::getVertexCount()
{
    return vertexCount;
}

VkBuffer Mesh::getVertexBuffer()
{
    return vertexBuffer;
}

int Mesh::getIndexCount()
{
    return indexCount;
}

VkBuffer Mesh::getIndexBuffer()
{
    return indexBuffer;
}

void Mesh::destroyBuffers()
{
    vkDestroyBuffer(device, vertexBuffer, nullptr);
    vkFreeMemory(device, vertexBufferMemory, nullptr);
    vkDestroyBuffer(device, indexBuffer, nullptr);
    vkFreeMemory(device, indexBufferMemory, nullptr);
}

void Mesh::createVertexBuffer(VkQueue transferQueue, VkCommandPool transferCommandPool)
{
    // Get size of buffer needed for vertices
    VkDeviceSize bufferSize = sizeof(Vertex) * vertices.size();

    // Temporary buffer to "stage" vertex data before transferring to GPU
    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;

    // Create Staging Buffer and Allocate Memory to it
    createBuffer(physicalDevice, device, bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
        &stagingBuffer, &stagingBufferMemory);

    // MAP MEMORY TO VERTEX BUFFER
    void * data;                                                                // 1. Create pointer to a point in normal memory
    vkMapMemory(device, stagingBufferMemory, 0, bufferSize, 0, &data);          // 2. "Map" the vertex buffer memory to that point
    memcpy(data, vertices.data(), (size_t)bufferSize);                         // 3. Copy memory from vertices vector to the point
    vkUnmapMemory(device, stagingBufferMemory);                                 // 4. Unmap the vertex buffer memory

    // Create buffer with TRANSFER_DST_BIT to mark as recipient of transfer data (also VERTEX_BUFFER)
    // Buffer memory is to be DEVICE_LOCAL_BIT meaning memory is on the GPU and only accessible by it and not CPU (host)
    createBuffer(physicalDevice, device, bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &vertexBuffer, &vertexBufferMemory);

    // Copy staging buffer to vertex buffer on GPU
    copyBuffer(device, transferQueue, transferCommandPool, stagingBuffer, vertexBuffer, bufferSize);

    // Clean up staging buffer parts
    vkDestroyBuffer(device, stagingBuffer, nullptr);
    vkFreeMemory(device, stagingBufferMemory, nullptr);
}

void Mesh::createIndexBuffer(VkQueue transferQueue, VkCommandPool transferCommandPool)
{
    // Get size of buffer needed for indices
    VkDeviceSize bufferSize = sizeof(uint32_t) * indices.size();
    
    // Temporary buffer to "stage" index data before transferring to GPU
    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    createBuffer(physicalDevice, device, bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, &stagingBuffer, &stagingBufferMemory);

    // MAP MEMORY TO INDEX BUFFER
    void * data;                                                                
    vkMapMemory(device, stagingBufferMemory, 0, bufferSize, 0, &data);          
    memcpy(data, indices.data(), (size_t)bufferSize);                          
    vkUnmapMemory(device, stagingBufferMemory);

    // Create buffer for INDEX data on GPU access only area
    createBuffer(physicalDevice, device, bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &indexBuffer, &indexBufferMemory);

    // Copy from staging buffer to GPU access buffer
    copyBuffer(device, transferQueue, transferCommandPool, stagingBuffer, indexBuffer, bufferSize);

    // Destroy + Release Staging Buffer resources
    vkDestroyBuffer(device, stagingBuffer, nullptr);
    vkFreeMemory(device, stagingBufferMemory, nullptr);
}
