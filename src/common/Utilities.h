#pragma once


const int MAX_OBJECTS = 40;
const int MAX_FRAME_DRAWS = 2;

// Vertex data representation
struct Vertex
{
    glm::vec3 pos; // Vertex Position (x, y, z)
    glm::vec3 col; // Vertex Colour (r, g, b)
    glm::vec2 tex; // Texture Coords (u, v)

    bool operator==(const Vertex& other)
    {
        return pos == other.pos;
    }

    friend bool operator<(const Vertex& lhs, const Vertex& rhs)
    {
        return std::tie(lhs.pos.x, lhs.pos.y, lhs.pos.z) < std::tie(rhs.pos.x, rhs.pos.y, rhs.pos.z);
    }
};

std::vector<char> readFile(const std::string& filename);

uint32_t findMemoryTypeIndex(VkPhysicalDevice physicalDevice, uint32_t allowedTypes, VkMemoryPropertyFlags properties);

void createBuffer(VkPhysicalDevice physicalDevice, VkDevice device, VkDeviceSize bufferSize, VkBufferUsageFlags bufferUsage,
    VkMemoryPropertyFlags bufferProperties, VkBuffer* buffer, VkDeviceMemory* bufferMemory);

void copyBuffer(VkDevice device, VkQueue transferQueue, VkCommandPool transferCommandPool,
    VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize bufferSize);

void copyImageBuffer(VkDevice device, VkQueue transferQueue, VkCommandPool transferCommandPool,
    VkBuffer srcBuffer, VkImage image, uint32_t width, uint32_t height);

void transitionImageLayout(VkDevice device, VkQueue queue, VkCommandPool commandPool,
    VkImage image, VkImageLayout oldLayout, VkImageLayout newLayout);