
#include "includes.h" // precompiled header first

#include <assert.h>

#include "ModelTree.h"

using namespace std;

ModelTree::ModelTree()
{
    model = glm::mat4(1.0f);
    transform = glm::mat4(1.0f);
}

void ModelTree::addMesh(const Mesh& newMesh)
{
    meshList.push_back(newMesh);
}

void ModelTree::loadTree(VkPhysicalDevice newPhysicalDevice, VkDevice newDevice,
    VkQueue transferQueue, VkCommandPool transferCommandPool,
    map<string, int>& textureIndices)
{
    for (auto& modelTree : children)
    {
        modelTree.loadTree(newPhysicalDevice, newDevice, transferQueue, transferCommandPool, textureIndices);
    }

    for (auto& mesh : meshList)
    {
        auto findTex = textureIndices.find(mesh.getMaterial());
        assert(findTex != textureIndices.end());

        mesh.loadMesh(newPhysicalDevice, newDevice, transferQueue, transferCommandPool, findTex->second);
    }
}

void ModelTree::drawTree(VkCommandBuffer commandBuffer, VkPipelineLayout pipelineLayout, 
    VkDescriptorSet descriptorSet, const std::vector<VkDescriptorSet>& samplerDescriptors)
{
    for (auto& modelTree : children)
    {
        modelTree.drawTree(commandBuffer, pipelineLayout, descriptorSet, samplerDescriptors);
    }

    glm::mat4 newModel = transform * model;

    // "Push" constants to given shader stage directly (no buffer)
    vkCmdPushConstants(
        commandBuffer,
        pipelineLayout,
        VK_SHADER_STAGE_VERTEX_BIT,     // Stage to push constants to
        0,                              // Offset of push constants to update
        sizeof(Model),                  // Size of data being pushed
        &newModel);     // Actual data being pushed (can be array)

    for (auto& mesh : meshList)
    {
        VkBuffer vertexBuffers[] = { mesh.getVertexBuffer() };                 // Buffers to bind
        VkDeviceSize offsets[] = { 0 };                                             // Offsets into buffers being bound
        vkCmdBindVertexBuffers(commandBuffer, 0, 1, vertexBuffers, offsets); // Command to bind vertex buffer before drawing with them

        // Bind mesh index buffer, with 0 offset and using the uint32 type
        vkCmdBindIndexBuffer(commandBuffer, mesh.getIndexBuffer(), 0, VK_INDEX_TYPE_UINT32);

        // Dynamic Offset Amount
        // uint32_t dynamicOffset = static_cast<uint32_t>(modelUniformAlignment) * j;

        array<VkDescriptorSet, 2> descriptorSetGroup = { descriptorSet,
            samplerDescriptors[mesh.getTexId()] };

        // Bind Descriptor Sets
        vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout,
            0, static_cast<uint32_t>(descriptorSetGroup.size()), descriptorSetGroup.data(), 0, nullptr);

        // Execute pipeline
        vkCmdDrawIndexed(commandBuffer, mesh.getIndexCount(), 1, 0, 0, 0);
    }
}

void ModelTree::addChild(const ModelTree& modelTree)
{
    children.emplace_back(modelTree);
}

size_t ModelTree::getMeshCount()
{
    return meshList.size();
}

Mesh* ModelTree::getMesh(size_t index)
{
    THROW_IF(index >= meshList.size(), "Attempted to access invalid Mesh index!");

    return &meshList[index];
}

glm::mat4 ModelTree::getModel()
{
    return model;
}

void ModelTree::setTransform(const glm::mat4& tf)
{
    for (auto& child : children)
    {
        child.setTransform(tf);
    }

    transform = tf;
}

void ModelTree::applyTransform(const glm::mat4& transform)
{
    for (auto& child : children)
    {
        child.applyTransform(transform);
    }

    model *= transform;
}

void ModelTree::rotate(float angle, const glm::vec3& axis)
{
    for (auto& child : children)
    {
        child.rotate(angle, axis);
    }

    model = glm::rotate(model, angle, axis);
}

void ModelTree::destroyMeshModel()
{
    for (auto& model : children)
    {
        model.destroyMeshModel();
    }

    for (auto& mesh : meshList)
    {
        mesh.destroyBuffers();
    }
}
