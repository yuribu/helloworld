#pragma once

#include "SwapChain.h"

class RenderPass : public SwapChain
{
public:
    RenderPass(GLFWwindow* newWindow);
    ~RenderPass();

protected:
    VkRenderPass mRenderPass;

    VkFormat mColorAttachmentFormat;
    VkFormat mDepthAttachmentFormat;

private:
    VkFormat chooseSupportedFormat(const std::vector<VkFormat>& formats,
        VkImageTiling tiling, VkFormatFeatureFlags featureFlags);
};