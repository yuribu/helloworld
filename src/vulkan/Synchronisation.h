#pragma once

#include "Descriptors.h"

class Synchronisation : public Descriptors
{
public:
    Synchronisation(GLFWwindow* newWindow);
    ~Synchronisation();

protected:
    std::vector<VkSemaphore> mImageAvailable;
    std::vector<VkSemaphore> mRenderFinished;
    std::vector<VkFence> mDrawFences;
};