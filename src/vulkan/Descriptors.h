#pragma once

#include <stb_image.h>

#include "Buffers.h"

struct Descriptors : public Buffers
{
public:
    Descriptors(GLFWwindow* newWindow);
    ~Descriptors();

protected:
    VkSampler mTextureSampler;
    std::vector<ImageBuffer> mTextureBuffers;

    VkDescriptorPool mDescriptorPool;
    VkDescriptorPool mSamplerDescriptorPool;
    VkDescriptorPool mInputDescriptorPool;
    std::vector<VkDescriptorSet> mDescriptorSets;
    std::vector<VkDescriptorSet> mSamplerDescriptorSets;
    std::vector<VkDescriptorSet> mInputDescriptorSets;

    int createTexture(const std::string& fileName);
    int createTextureFromColor(const glm::vec4& color);

private:
    void createTextureSampler();
    void createDescriptorPool();
    void createDescriptorSets();
    void createInputDescriptorSets();

    ImageBuffer createTextureImage(void* imageData, int width, int height, VkDeviceSize imageSize);
    int createTextureDescriptor(VkImageView textureImage);

    // -- Loader Functions
    stbi_uc* loadTextureFile(const std::string& fileName, int* width, int* height, VkDeviceSize* imageSize);
};