#pragma once

#include "Instance.h"

class Device : public Instance
{
public:
    Device(GLFWwindow* newWindow);
    ~Device();

protected:
    struct {
        VkPhysicalDevice physicalDevice;
        VkDevice logicalDevice;
    } mDevice;

    struct SwapChainDetails {
        VkSurfaceCapabilitiesKHR surfaceCapabilities;       // Surface properties, e.g. image size/extent
        std::vector<VkSurfaceFormatKHR> formats;            // Surface image formats, e.g. RGBA and size of each colour
        std::vector<VkPresentModeKHR> presentationModes;    // How images should be presented to screen
    };

    // Indices (locations) of Queue Families (if they exist at all)
    struct QueueFamilyIndices {
        int graphicsFamily = -1;            // Location of Graphics Queue Family
        int presentationFamily = -1;        // Location of Presentation Queue Family

        // Check if queue families are valid
        bool isValid()
        {
            return graphicsFamily >= 0 && presentationFamily >= 0;
        }
    };

    QueueFamilyIndices getQueueFamilies(VkPhysicalDevice device);
    SwapChainDetails getSwapChainDetails(VkPhysicalDevice device);

    VkSurfaceKHR mSurface;
    VkQueue mGraphicsQueue;
    VkQueue mPresentationQueue;

    GLFWwindow* mWindow;

private:
    bool checkDeviceExtensionSupport(VkPhysicalDevice device);
    bool checkDeviceSuitable(VkPhysicalDevice device);

};