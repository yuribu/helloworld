
#include "../common/includes.h" // precompiled header first

#include "../dae/DaeParser.h"

#include "VulkanRenderer.h"

using namespace std;

VulkanRenderer::VulkanRenderer(GLFWwindow* newWindow) : Synchronisation(newWindow)
{
    uboViewProjection.projection = glm::perspective(glm::radians(45.0f), (float)mSwapChainExtent.width / (float)mSwapChainExtent.height, 0.1f, 100.0f);
    uboViewProjection.view = glm::lookAt(glm::vec3(10.0f, 0.0f, 20.0f), glm::vec3(0.0f, 0.0f, -2.0f), glm::vec3(0.0f, 1.0f, 0.0f));

    uboViewProjection.projection[1][1] *= -1;
}

VulkanRenderer::~VulkanRenderer()
{
    // Wait until no actions being run on device before destroying
    vkDeviceWaitIdle(mDevice.logicalDevice);

    for (size_t i = 0; i < modelList.size(); i++)
    {
        modelList[i].destroyMeshModel();
    }
}

void VulkanRenderer::setModelsTransform(const glm::mat4& transform)
{
    for (auto& model : modelList)
    {
        model.setTransform(transform);
    }
}


void VulkanRenderer::draw()
{
    // -- GET NEXT IMAGE --
    // Wait for given fence to signal (open) from last draw before continuing
    vkWaitForFences(mDevice.logicalDevice, 1, &mDrawFences[currentFrame], VK_TRUE, numeric_limits<uint64_t>::max());
    // Manually reset (close) fences
    vkResetFences(mDevice.logicalDevice, 1, &mDrawFences[currentFrame]);

    // Get index of next image to be drawn to, and signal semaphore when ready to be drawn to
    uint32_t imageIndex;
    vkAcquireNextImageKHR(mDevice.logicalDevice, mSwapchain, numeric_limits<uint64_t>::max(), mImageAvailable[currentFrame], VK_NULL_HANDLE, &imageIndex);
    
    recordCommands(imageIndex);
    updateUniformBuffers(imageIndex);

    // -- SUBMIT COMMAND BUFFER TO RENDER --
    // Queue submission information
    VkSubmitInfo submitInfo = {};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.waitSemaphoreCount = 1;                                      // Number of semaphores to wait on
    submitInfo.pWaitSemaphores = &mImageAvailable[currentFrame];             // List of semaphores to wait on
    VkPipelineStageFlags waitStages[] = {
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
    };
    submitInfo.pWaitDstStageMask = waitStages;                      // Stages to check semaphores at
    submitInfo.commandBufferCount = 1;                              // Number of command buffers to submit
    submitInfo.pCommandBuffers = &mCommandBuffers[imageIndex];       // Command buffer to submit
    submitInfo.signalSemaphoreCount = 1;                            // Number of semaphores to signal
    submitInfo.pSignalSemaphores = &mRenderFinished[currentFrame];   // Semaphores to signal when command buffer finishes

    // Submit command buffer to queue
    VkResult result = vkQueueSubmit(mGraphicsQueue, 1, &submitInfo, mDrawFences[currentFrame]);
    THROW_IF(result != VK_SUCCESS, "Failed to submit Command Buffer to Queue!");


    // -- PRESENT RENDERED IMAGE TO SCREEN --
    VkPresentInfoKHR presentInfo = {};
    presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    presentInfo.waitSemaphoreCount = 1;                                     // Number of semaphores to wait on
    presentInfo.pWaitSemaphores = &mRenderFinished[currentFrame];            // Semaphores to wait on
    presentInfo.swapchainCount = 1;                                         // Number of swapchains to present to
    presentInfo.pSwapchains = &mSwapchain;                                   // Swapchains to present images to
    presentInfo.pImageIndices = &imageIndex;                                // Index of images in swapchains to present

    // Present image
    result = vkQueuePresentKHR(mPresentationQueue, &presentInfo);
    THROW_IF(result != VK_SUCCESS, "Failed to present Image!");

    // Get next frame (use % swapChainImages.size() to keep value below swapChainImages.size())
    currentFrame = (currentFrame + 1) % MAX_FRAME_DRAWS;
}

void VulkanRenderer::recordCommands(uint32_t currentImage)
{
    // Information about how to begin each command buffer
    VkCommandBufferBeginInfo bufferBeginInfo = {};
    bufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

    // Information about how to begin a render pass (only needed for graphical applications)
    VkRenderPassBeginInfo renderPassBeginInfo = {};
    renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassBeginInfo.renderPass = mRenderPass;                            // Render Pass to begin
    renderPassBeginInfo.renderArea.offset = { 0, 0 };                       // Start point of render pass in pixels
    renderPassBeginInfo.renderArea.extent = mSwapChainExtent;                // Size of region to run render pass on (starting at offset)

    std::array<VkClearValue, 3> clearValues = {};
    clearValues[0].color = { 0.0f, 0.0f, 0.0f, 1.0f };
    clearValues[1].color = { 0.6f, 0.65f, 0.4f, 1.0f };
    clearValues[2].depthStencil.depth = 1.0f;

    renderPassBeginInfo.pClearValues = clearValues.data();                  // List of clear values
    renderPassBeginInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());

    renderPassBeginInfo.framebuffer = mSwapChainFramebuffers[currentImage];

    // Start recording commands to command buffer!
    VkResult result = vkBeginCommandBuffer(mCommandBuffers[currentImage], &bufferBeginInfo);
    if (result != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to start recording a Command Buffer!");
    }

        // Begin Render Pass
        vkCmdBeginRenderPass(mCommandBuffers[currentImage], &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

            // Bind Pipeline to be used in render pass
            vkCmdBindPipeline(mCommandBuffers[currentImage], VK_PIPELINE_BIND_POINT_GRAPHICS, mGraphicsPipeline);

            for (auto& model : modelList)
            {
                model.drawTree(mCommandBuffers[currentImage], mPipelineLayout, 
                    mDescriptorSets[currentImage], mSamplerDescriptorSets);
            }

            // Start second subpass
            vkCmdNextSubpass(mCommandBuffers[currentImage], VK_SUBPASS_CONTENTS_INLINE);

            vkCmdBindPipeline(mCommandBuffers[currentImage], VK_PIPELINE_BIND_POINT_GRAPHICS, mSecondPipeline);
            vkCmdBindDescriptorSets(mCommandBuffers[currentImage], VK_PIPELINE_BIND_POINT_GRAPHICS, mSecondPipelineLayout,
                0, 1, &mInputDescriptorSets[currentImage], 0, nullptr);
            vkCmdDraw(mCommandBuffers[currentImage], 3, 1, 0, 0);

        // End Render Pass
        vkCmdEndRenderPass(mCommandBuffers[currentImage]);

    // Stop recording to command buffer
    result = vkEndCommandBuffer(mCommandBuffers[currentImage]);
    if (result != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to stop recording a Command Buffer!");
    }
    
}

void VulkanRenderer::createMeshModel(const std::string& modelFile)
{
    DaeParser daeParser;

    daeParser.parse(modelFile, modelList);

    map<string, int> textureIndices;

    // create texture from file
    for (auto& texFile : daeParser.getTextureFiles())
    {
        textureIndices[texFile.first] = createTexture(texFile.second);
    }

    // create texture from color
    for (auto& texCol : daeParser.getTextureColors())
    {
        textureIndices[texCol.first] = createTextureFromColor(texCol.second);
    }

    for (auto& model : modelList)
    {
        model.loadTree(mDevice.physicalDevice, mDevice.logicalDevice,
            mGraphicsQueue, mGraphicsCommandPool, textureIndices);
    }
}
