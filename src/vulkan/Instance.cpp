
#include "../common/includes.h" // precompiled header first

#include <iostream>

#include "Instance.h"

using namespace std;

const bool validationEnabled = true;

// List of validation layers to use
const vector<const char*> validationLayers = {
    "VK_LAYER_KHRONOS_validation",
};

// Callback function for validation debugging (will be called when validation information record)
static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
    VkDebugReportFlagsEXT flags,                // Type of error
    VkDebugReportObjectTypeEXT objType,         // Type of object causing error
    uint64_t obj,                               // ID of object
    size_t location,
    int32_t code,
    const char* layerPrefix,
    const char* message,                       // Validation Information
    void* userData)
{
    // If validation ERROR, then output error and return failure
    if (flags & VK_DEBUG_REPORT_ERROR_BIT_EXT)
    {
        printf("VALIDATION ERROR: %s\n", message);
        return VK_TRUE;
    }

    // If validation WARNING, then output warning and return okay
    if (flags & VK_DEBUG_REPORT_WARNING_BIT_EXT)
    {
        printf("VALIDATION WARNING: %s\n", message);
        return VK_FALSE;
    }

    return VK_FALSE;
}

static VkResult CreateDebugReportCallbackEXT(VkInstance instance, const VkDebugReportCallbackCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugReportCallbackEXT* pCallback)
{
    // vkGetInstanceProcAddr returns a function pointer to the requested function in the requested instance
    // resulting function is cast as a function pointer with the header of "vkCreateDebugReportCallbackEXT"
    auto func = (PFN_vkCreateDebugReportCallbackEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT");

    // If function was found, executre if with given data and return result, otherwise, return error
    if (func != nullptr)
    {
        return func(instance, pCreateInfo, pAllocator, pCallback);
    }
    else
    {
        return VK_ERROR_EXTENSION_NOT_PRESENT;
    }
}

static void DestroyDebugReportCallbackEXT(VkInstance instance, VkDebugReportCallbackEXT callback, const VkAllocationCallbacks* pAllocator)
{
    // get function pointer to requested function, then cast to function pointer for vkDestroyDebugReportCallbackEXT
    auto func = (PFN_vkDestroyDebugReportCallbackEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT");

    // If function found, execute
    if (func != nullptr)
    {
        func(instance, callback, pAllocator);
    }
}

Instance::Instance()
{
    THROW_IF(validationEnabled && !checkValidationLayerSupport(), "Required Validation Layers not supported!");

    // Information about the application itself
    // Most data here doesn't affect the program and is for developer convenience
    VkApplicationInfo appInfo = {};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = "Vulkan App";                    // Custom name of the application
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);      // Custom version of the application
    appInfo.pEngineName = "No Engine";                          // Custom engine name
    appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);           // Custom engine version
    appInfo.apiVersion = VK_API_VERSION_1_0;                    // The Vulkan Version

    // Creation information for a VkInstance (Vulkan Instance)
    VkInstanceCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = &appInfo;

    // Create list to hold instance extensions
    vector<const char*> instanceExtensions;

    // Set up extensions Instance will use
    uint32_t glfwExtensionCount = 0;                // GLFW may require multiple extensions
    const char** glfwExtensions;                    // Extensions passed as array of cstrings, so need pointer (the array) to pointer (the cstring)

    // Get GLFW extensions
    glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

    // Add GLFW extensions to list of extensions
    for (size_t i = 0; i < glfwExtensionCount; i++)
    {
        instanceExtensions.push_back(glfwExtensions[i]);
    }

    // If validation enabled, add extension to report validation debug info
    if (validationEnabled)
    {
        instanceExtensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
    }

    // Check Instance Extensions supported...
    THROW_IF(!checkInstanceExtensionSupport(&instanceExtensions), "VkInstance does not support required extensions!");

    createInfo.enabledExtensionCount = static_cast<uint32_t>(instanceExtensions.size());
    createInfo.ppEnabledExtensionNames = instanceExtensions.data();

    if (validationEnabled)
    {
        createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
        createInfo.ppEnabledLayerNames = validationLayers.data();
    }
    else
    {
        createInfo.enabledLayerCount = 0;
        createInfo.ppEnabledLayerNames = nullptr;
    }

    // Create instance
    VkResult result = vkCreateInstance(&createInfo, nullptr, &mInstance);

    THROW_IF(result != VK_SUCCESS, "Failed to create a Vulkan Instance!");

    if (validationEnabled)
    {
        VkDebugReportCallbackCreateInfoEXT callbackCreateInfo = {};
        callbackCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
        callbackCreateInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT; // Which validation reports should initiate callback
        callbackCreateInfo.pfnCallback = debugCallback;                                             // Pointer to callback function itself

        // Create debug callback with custom create function
        VkResult result = CreateDebugReportCallbackEXT(mInstance, &callbackCreateInfo, nullptr, &callback);
        THROW_IF(result != VK_SUCCESS, "Failed to create Debug Callback!");
    }
}

Instance::~Instance()
{
    if (validationEnabled)
    {
        DestroyDebugReportCallbackEXT(mInstance, callback, nullptr);
    }
    vkDestroyInstance(mInstance, nullptr);
}

bool Instance::checkValidationLayerSupport() const
{
    // Get number of validation layers to create vector of appropriate size
    uint32_t validationLayerCount;
    vkEnumerateInstanceLayerProperties(&validationLayerCount, nullptr);

    // Check if no validation layers found AND we want at least 1 layer
    if (validationLayerCount == 0 && validationLayers.size() > 0)
    {
        return false;
    }

    vector<VkLayerProperties> availableLayers(validationLayerCount);
    vkEnumerateInstanceLayerProperties(&validationLayerCount, availableLayers.data());

    // Check if given Validation Layer is in list of given Validation Layers
    for (const auto& validationLayer : validationLayers)
    {
        bool hasLayer = false;
        for (const auto& availableLayer : availableLayers)
        {
            if (strcmp(validationLayer, availableLayer.layerName) == 0)
            {
                hasLayer = true;
                break;
            }
        }

        if (!hasLayer)
        {
            return false;
        }
    }

    return true;
}

bool Instance::checkInstanceExtensionSupport(std::vector<const char*>* checkExtensions)
{
    // Need to get number of extensions to create array of correct size to hold extensions
    uint32_t extensionCount = 0;
    vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);

    // Create a list of VkExtensionProperties using count
    std::vector<VkExtensionProperties> extensions(extensionCount);
    vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, extensions.data());

    // Check if given extensions are in list of available extensions
    for (const auto& checkExtension : *checkExtensions)
    {
        bool hasExtension = false;
        for (const auto& extension : extensions)
        {
            if (strcmp(checkExtension, extension.extensionName) == 0)
            {
                hasExtension = true;
                break;
            }
        }

        if (!hasExtension)
        {
            return false;
        }
    }

    return true;
}