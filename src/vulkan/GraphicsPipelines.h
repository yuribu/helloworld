#pragma once

#include "RenderPass.h"

class GraphicsPipelines : public RenderPass
{
public:
    GraphicsPipelines(GLFWwindow* newWindow);
    ~GraphicsPipelines();

protected:
    // - Descriptors
    VkDescriptorSetLayout mDescriptorSetLayout;
    VkDescriptorSetLayout mInputSetLayout;
    VkDescriptorSetLayout mSamplerSetLayout;
    VkPushConstantRange mPushConstantRange;

    // - Pipelines
    VkPipeline mGraphicsPipeline;
    VkPipelineLayout mPipelineLayout;

    VkPipeline mSecondPipeline;
    VkPipelineLayout mSecondPipelineLayout;

private:
    void createDescriptorSetLayout();
    void createPushConstantRange();
    void createGraphicsPipelines();

    VkShaderModule createShaderModule(const std::vector<char>& code);
};