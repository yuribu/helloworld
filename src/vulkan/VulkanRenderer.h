#pragma once

#include "../common/ModelTree.h"
#include "../common/Utilities.h"

#include "Synchronisation.h"

// create sequence: Instance -> Device -> SwapChain -> RenderPass -> GraphicsPipelines -> 
//                  Buffers -> Descriptors -> Synchronisation -> VulkanRenderer
class VulkanRenderer : public Synchronisation
{
public:
    VulkanRenderer(GLFWwindow* newWindow);
    ~VulkanRenderer();

    void createMeshModel(const std::string& modelFile);

    void setModelsTransform(const glm::mat4& transform);

    void draw();

private:
    int currentFrame = 0;

    // Scene Objects
    std::vector<ModelTree> modelList;

    // - Record Functions
    void recordCommands(uint32_t currentImage);
};

