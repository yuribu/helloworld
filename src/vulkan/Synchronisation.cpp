
#include "../common/includes.h" // precompiled header must be first

#include "../common/Utilities.h"

#include "Synchronisation.h"

Synchronisation::Synchronisation(GLFWwindow* newWindow) : Descriptors(newWindow)
{
    mImageAvailable.resize(MAX_FRAME_DRAWS);
    mRenderFinished.resize(MAX_FRAME_DRAWS);
    mDrawFences.resize(MAX_FRAME_DRAWS);

    // Semaphore creation information
    VkSemaphoreCreateInfo semaphoreCreateInfo = {};
    semaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    // Fence creation information
    VkFenceCreateInfo fenceCreateInfo = {};
    fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceCreateInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    for (size_t i = 0; i < MAX_FRAME_DRAWS; i++)
    {
        if (vkCreateSemaphore(mDevice.logicalDevice, &semaphoreCreateInfo, nullptr, &mImageAvailable[i]) != VK_SUCCESS ||
            vkCreateSemaphore(mDevice.logicalDevice, &semaphoreCreateInfo, nullptr, &mRenderFinished[i]) != VK_SUCCESS ||
            vkCreateFence(mDevice.logicalDevice, &fenceCreateInfo, nullptr, &mDrawFences[i]) != VK_SUCCESS)
        {
            THROW("Failed to create a Semaphore and/or Fence!");
        }
    }
}

Synchronisation::~Synchronisation()
{
    for (size_t i = 0; i < MAX_FRAME_DRAWS; i++)
    {
        vkDestroySemaphore(mDevice.logicalDevice, mRenderFinished[i], nullptr);
        vkDestroySemaphore(mDevice.logicalDevice, mImageAvailable[i], nullptr);
        vkDestroyFence(mDevice.logicalDevice, mDrawFences[i], nullptr);
    }
}
