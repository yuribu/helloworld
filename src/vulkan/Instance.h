#pragma once

class Instance
{
public:
    Instance();
    ~Instance();

protected:
    VkInstance mInstance;

private:
    VkDebugReportCallbackEXT callback;

    bool checkValidationLayerSupport() const;
    bool checkInstanceExtensionSupport(std::vector<const char*>* checkExtensions);
};