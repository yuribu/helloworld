
#include "../common/includes.h" // precompiled header first

#include "../common/Utilities.h"

#include "Descriptors.h"

using namespace std;

Descriptors::Descriptors(GLFWwindow* newWindow) : Buffers(newWindow)
{
    createTextureSampler();
    createDescriptorPool();
    createDescriptorSets();
    createInputDescriptorSets();
}

Descriptors::~Descriptors()
{
    vkDestroyDescriptorPool(mDevice.logicalDevice, mInputDescriptorPool, nullptr);
    vkDestroyDescriptorPool(mDevice.logicalDevice, mSamplerDescriptorPool, nullptr);
    vkDestroyDescriptorPool(mDevice.logicalDevice, mDescriptorPool, nullptr);

    for (size_t i = 0; i < mTextureBuffers.size(); i++)
    {
        vkDestroyImageView(mDevice.logicalDevice, mTextureBuffers[i].imageView, nullptr);
        vkDestroyImage(mDevice.logicalDevice, mTextureBuffers[i].image, nullptr);
        vkFreeMemory(mDevice.logicalDevice, mTextureBuffers[i].imageMemory, nullptr);
    }

    vkDestroySampler(mDevice.logicalDevice, mTextureSampler, nullptr);
}

void Descriptors::createTextureSampler()
{
    // Sampler Creation Info
    VkSamplerCreateInfo samplerCreateInfo = {};
    samplerCreateInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    samplerCreateInfo.magFilter = VK_FILTER_LINEAR;                     // How to render when image is magnified on screen
    samplerCreateInfo.minFilter = VK_FILTER_LINEAR;                     // How to render when image is minified on screen
    samplerCreateInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;    // How to handle texture wrap in U (x) direction
    samplerCreateInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;    // How to handle texture wrap in V (y) direction
    samplerCreateInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;    // How to handle texture wrap in W (z) direction
    samplerCreateInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;   // Border beyond texture (only workds for border clamp)
    samplerCreateInfo.unnormalizedCoordinates = VK_FALSE;               // Whether coords should be normalized (between 0 and 1)
    samplerCreateInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;       // Mipmap interpolation mode
    samplerCreateInfo.mipLodBias = 0.0f;                                // Level of Details bias for mip level
    samplerCreateInfo.minLod = 0.0f;                                    // Minimum Level of Detail to pick mip level
    samplerCreateInfo.maxLod = 0.0f;                                    // Maximum Level of Detail to pick mip level
    samplerCreateInfo.anisotropyEnable = VK_TRUE;                       // Enable Anisotropy
    samplerCreateInfo.maxAnisotropy = 16;                               // Anisotropy sample level

    VkResult result = vkCreateSampler(mDevice.logicalDevice, &samplerCreateInfo, nullptr, &mTextureSampler);
    THROW_IF(result != VK_SUCCESS, "Filed to create a Texture Sampler!");
}

void Descriptors::createDescriptorPool()
{
    // CREATE UNIFORM DESCRIPTOR POOL
    // Type of descriptors + how many DESCRIPTORS, not Descriptor Sets (combined makes the pool size)
    // ViewProjection Pool
    VkDescriptorPoolSize vpPoolSize = {};
    vpPoolSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    vpPoolSize.descriptorCount = static_cast<uint32_t>(mVpUniformBuffer.size());

    // Model Pool (DYNAMIC)
    /*VkDescriptorPoolSize modelPoolSize = {};
    modelPoolSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
    modelPoolSize.descriptorCount = static_cast<uint32_t>(modelDUniformBuffer.size());*/

    // List of pool sizes
    std::vector<VkDescriptorPoolSize> descriptorPoolSizes = { vpPoolSize };

    // Data to create Descriptor Pool
    VkDescriptorPoolCreateInfo poolCreateInfo = {};
    poolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    poolCreateInfo.maxSets = static_cast<uint32_t>(mSwapChainImages.size());                 // Maximum number of Descriptor Sets that can be created from pool
    poolCreateInfo.poolSizeCount = static_cast<uint32_t>(descriptorPoolSizes.size());       // Amount of Pool Sizes being passed
    poolCreateInfo.pPoolSizes = descriptorPoolSizes.data();                                 // Pool Sizes to create pool with

    // Create Descriptor Pool
    VkResult result = vkCreateDescriptorPool(mDevice.logicalDevice, &poolCreateInfo, nullptr, &mDescriptorPool);
    THROW_IF(result != VK_SUCCESS, "Failed to create a Descriptor Pool!");

    // CREATE SAMPLER DESCRIPTOR POOL
    // Texture sampler pool
    VkDescriptorPoolSize samplerPoolSize = {};
    samplerPoolSize.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    samplerPoolSize.descriptorCount = MAX_OBJECTS;

    VkDescriptorPoolCreateInfo samplerPoolCreateInfo = {};
    samplerPoolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    samplerPoolCreateInfo.maxSets = MAX_OBJECTS;
    samplerPoolCreateInfo.poolSizeCount = 1;
    samplerPoolCreateInfo.pPoolSizes = &samplerPoolSize;

    result = vkCreateDescriptorPool(mDevice.logicalDevice, &samplerPoolCreateInfo, nullptr, &mSamplerDescriptorPool);
    THROW_IF(result != VK_SUCCESS, "Failed to create a Descriptor Pool!");

    // CREATE INPUT ATTACHMENT DESCRIPTOR POOL
    // Color Attachment Pool Size
    VkDescriptorPoolSize colorInputPoolSize = {};
    colorInputPoolSize.type = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
    colorInputPoolSize.descriptorCount = static_cast<uint32_t>(mColorBuffers.size());

    // Depth Attachment Pool Size
    VkDescriptorPoolSize depthInputPoolSize = {};
    depthInputPoolSize.type = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
    depthInputPoolSize.descriptorCount = static_cast<uint32_t>(mDepthBuffers.size());

    std::vector<VkDescriptorPoolSize> inputPoolSizes = { colorInputPoolSize, depthInputPoolSize };

    // Create input attachment pool
    VkDescriptorPoolCreateInfo inputPoolCreateInfo = {};
    inputPoolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    inputPoolCreateInfo.maxSets = static_cast<uint32_t>(mSwapChainImages.size());
    inputPoolCreateInfo.poolSizeCount = static_cast<uint32_t>(inputPoolSizes.size());
    inputPoolCreateInfo.pPoolSizes = inputPoolSizes.data();

    result = vkCreateDescriptorPool(mDevice.logicalDevice, &inputPoolCreateInfo, nullptr, &mInputDescriptorPool);
    THROW_IF(result != VK_SUCCESS, "Failed to create a Descriptor Pool!");
}

void Descriptors::createDescriptorSets()
{
    // Resize Descriptor Set list so one for every buffer
    mDescriptorSets.resize(mSwapChainImages.size());

    std::vector<VkDescriptorSetLayout> setLayouts(mSwapChainImages.size(), mDescriptorSetLayout);

    // Descriptor Set Allocation Info
    VkDescriptorSetAllocateInfo setAllocInfo = {};
    setAllocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    setAllocInfo.descriptorPool = mDescriptorPool;                                   // Pool to allocate Descriptor Set from
    setAllocInfo.descriptorSetCount = static_cast<uint32_t>(mSwapChainImages.size());// Number of sets to allocate
    setAllocInfo.pSetLayouts = setLayouts.data();                                   // Layouts to use to allocate sets (1:1 relationship)

    // Allocate descriptor sets (multiple)
    VkResult result = vkAllocateDescriptorSets(mDevice.logicalDevice, &setAllocInfo, mDescriptorSets.data());
    THROW_IF(result != VK_SUCCESS, "Failed to allocate Descriptor Sets!");

    // Update all of descriptor set buffer bindings
    for (size_t i = 0; i < mSwapChainImages.size(); i++)
    {
        // VIEW PROJECTION DESCRIPTOR
        // Buffer info and data offset info
        VkDescriptorBufferInfo vpBufferInfo = {};
        vpBufferInfo.buffer = mVpUniformBuffer[i];       // Buffer to get data from
        vpBufferInfo.offset = 0;                        // Position of start of data
        vpBufferInfo.range = sizeof(UboViewProjection);             // Size of data

        // Data about connection between binding and buffer
        VkWriteDescriptorSet vpSetWrite = {};
        vpSetWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        vpSetWrite.dstSet = mDescriptorSets[i];                              // Descriptor Set to update
        vpSetWrite.dstBinding = 0;                                          // Binding to update (matches with binding on layout/shader)
        vpSetWrite.dstArrayElement = 0;                                 // Index in array to update
        vpSetWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;      // Type of descriptor
        vpSetWrite.descriptorCount = 1;                                 // Amount to update
        vpSetWrite.pBufferInfo = &vpBufferInfo;                         // Information about buffer data to bind

        // MODEL DESCRIPTOR
        // Model Buffer Binding Info
        /*VkDescriptorBufferInfo modelBufferInfo = {};
        modelBufferInfo.buffer = modelDUniformBuffer[i];
        modelBufferInfo.offset = 0;
        modelBufferInfo.range = modelUniformAlignment;

        VkWriteDescriptorSet modelSetWrite = {};
        modelSetWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        modelSetWrite.dstSet = descriptorSets[i];
        modelSetWrite.dstBinding = 1;
        modelSetWrite.dstArrayElement = 0;
        modelSetWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
        modelSetWrite.descriptorCount = 1;
        modelSetWrite.pBufferInfo = &modelBufferInfo;*/

        // List of Descriptor Set Writes
        vector<VkWriteDescriptorSet> setWrites = { vpSetWrite };

        // Update the descriptor sets with new buffer/binding info
        vkUpdateDescriptorSets(mDevice.logicalDevice, static_cast<uint32_t>(setWrites.size()), setWrites.data(),
            0, nullptr);
    }
}

void Descriptors::createInputDescriptorSets()
{
    // Resize array to hold descriptor set for each swap chain image
    mInputDescriptorSets.resize(mSwapChainImages.size());

    // Fill array of layouts ready for set creation
    vector<VkDescriptorSetLayout> setLayouts(mSwapChainImages.size(), mInputSetLayout);

    // Input Attachment Descriptor Set Allocation Info
    VkDescriptorSetAllocateInfo setAllocInfo = {};
    setAllocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    setAllocInfo.descriptorPool = mInputDescriptorPool;
    setAllocInfo.descriptorSetCount = static_cast<uint32_t>(mSwapChainImages.size());
    setAllocInfo.pSetLayouts = setLayouts.data();

    // Allocate Descriptor Sets
    VkResult result = vkAllocateDescriptorSets(mDevice.logicalDevice, &setAllocInfo, mInputDescriptorSets.data());
    THROW_IF(result != VK_SUCCESS, "Failed to allocate Input Attachment Descriptor Sets!");

    // Update each descriptor set with input attachment
    for (size_t i = 0; i < mSwapChainImages.size(); i++)
    {
        // Color Attachment Descriptor
        VkDescriptorImageInfo colorAttachmentDescriptor = {};
        colorAttachmentDescriptor.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        colorAttachmentDescriptor.imageView = mColorBuffers[i].imageView;
        colorAttachmentDescriptor.sampler = VK_NULL_HANDLE;

        // Color Attachment Descriptor Write
        VkWriteDescriptorSet colorWrite = {};
        colorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        colorWrite.dstSet = mInputDescriptorSets[i];
        colorWrite.dstBinding = 0;
        colorWrite.dstArrayElement = 0;
        colorWrite.descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
        colorWrite.descriptorCount = 1;
        colorWrite.pImageInfo = &colorAttachmentDescriptor;

        // Depth Attachment Descriptor
        VkDescriptorImageInfo depthAttachmentDescriptor = {};
        depthAttachmentDescriptor.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        depthAttachmentDescriptor.imageView = mDepthBuffers[i].imageView;
        depthAttachmentDescriptor.sampler = VK_NULL_HANDLE;

        // Depth Attachment Descriptor Write
        VkWriteDescriptorSet depthWrite = {};
        depthWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        depthWrite.dstSet = mInputDescriptorSets[i];
        depthWrite.dstBinding = 1;
        depthWrite.dstArrayElement = 0;
        depthWrite.descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
        depthWrite.descriptorCount = 1;
        depthWrite.pImageInfo = &depthAttachmentDescriptor;

        // List of input descriptor set writes
        vector<VkWriteDescriptorSet> setWrites = { colorWrite, depthWrite };

        // Update descriptor sets
        vkUpdateDescriptorSets(mDevice.logicalDevice, static_cast<uint32_t>(setWrites.size()), setWrites.data(), 0, nullptr);
    }
}

int Descriptors::createTexture(const string& fileName)
{
    // Create Texture Image and get its location in array
    //int textureImageLoc = createTextureImage(fileName);

    // Load image file
    int width, height;
    VkDeviceSize imageSize;
    stbi_uc* imageData = loadTextureFile(fileName, &width, &height, &imageSize);

    mTextureBuffers.emplace_back(createTextureImage(imageData, width, height, imageSize));

    // Free original image data
    stbi_image_free(imageData);

    // Create Image View and add to list
    mTextureBuffers.back().imageView = createImageView(mTextureBuffers.back().image, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_ASPECT_COLOR_BIT);

    // Create Texture Descriptor
    int descriptorLoc = createTextureDescriptor(mTextureBuffers.back().imageView);

    // Return location of set with texture
    return descriptorLoc;
}

int Descriptors::createTextureFromColor(const glm::vec4& color)
{
    // Create Texture Image and get its location in array
    //int textureImageLoc = createTextureImage(fileName);

    vector<float> pixel = { color.r, color.g, color.b, color.a };

    mTextureBuffers.emplace_back(createTextureImage(pixel.data(), 1, 1, pixel.size()));

    // Create Image View and add to list
    mTextureBuffers.back().imageView = createImageView(mTextureBuffers.back().image, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_ASPECT_COLOR_BIT);

    // Create Texture Descriptor
    int descriptorLoc = createTextureDescriptor(mTextureBuffers.back().imageView);

    // Return location of set with texture
    return descriptorLoc;
}

Buffers::ImageBuffer Descriptors::createTextureImage(void* imageData, int width, int height, VkDeviceSize imageSize)
{
    // Create staging buffer to hold loaded data, ready to copy to device
    VkBuffer imageStagingBuffer;
    VkDeviceMemory imageStagingBufferMemory;
    createBuffer(mDevice.physicalDevice, mDevice.logicalDevice, imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
        &imageStagingBuffer, &imageStagingBufferMemory);

    // Copy image data to staging buffer
    void* data;
    vkMapMemory(mDevice.logicalDevice, imageStagingBufferMemory, 0, imageSize, 0, &data);
    memcpy(data, imageData, static_cast<size_t>(imageSize));
    vkUnmapMemory(mDevice.logicalDevice, imageStagingBufferMemory);



    // Create image to hold final texture
    VkImage texImage;
    VkDeviceMemory texImageMemory;
    texImage = createImage(width, height, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TILING_OPTIMAL,
        VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &texImageMemory);


    // COPY DATA TO IMAGE
    // Transition image to be DST for copy operation
    transitionImageLayout(mDevice.logicalDevice, mGraphicsQueue, mGraphicsCommandPool,
        texImage, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);

    // Copy image data
    copyImageBuffer(mDevice.logicalDevice, mGraphicsQueue, mGraphicsCommandPool, imageStagingBuffer, texImage, width, height);

    // Transition image to be shader readable for shader usage
    transitionImageLayout(mDevice.logicalDevice, mGraphicsQueue, mGraphicsCommandPool,
        texImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    // Add texture data to vector for reference
    //textureImages.push_back(texImage);
    //textureImageMemory.push_back(texImageMemory);

    // Destroy staging buffers
    vkDestroyBuffer(mDevice.logicalDevice, imageStagingBuffer, nullptr);
    vkFreeMemory(mDevice.logicalDevice, imageStagingBufferMemory, nullptr);

    // Return new texture image
    //return textureImages.size() - 1;
    ImageBuffer imageBuffer = {};
    imageBuffer.image = texImage;
    imageBuffer.imageMemory = texImageMemory;
    return imageBuffer;
}

int Descriptors::createTextureDescriptor(VkImageView textureImage)
{
    VkDescriptorSet descriptorSet;

    // Descriptor Set Allocation Info
    VkDescriptorSetAllocateInfo setAllocInfo = {};
    setAllocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    setAllocInfo.descriptorPool = mSamplerDescriptorPool;
    setAllocInfo.descriptorSetCount = 1;
    setAllocInfo.pSetLayouts = &mSamplerSetLayout;

    // Allocate Descriptor Sets
    VkResult result = vkAllocateDescriptorSets(mDevice.logicalDevice, &setAllocInfo, &descriptorSet);
    THROW_IF(result != VK_SUCCESS, "Failed to allocate Texture Descriptor Sets!");

    // Texture Image Info
    VkDescriptorImageInfo imageInfo = {};
    imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;   // Image layout when in use
    imageInfo.imageView = textureImage;                                 // Image to bind to set
    imageInfo.sampler = mTextureSampler;                                 // Sampler to use for set

    // Descriptor Write Info
    VkWriteDescriptorSet descriptorWrite = {};
    descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrite.dstSet = descriptorSet;
    descriptorWrite.dstBinding = 0;
    descriptorWrite.dstArrayElement = 0;
    descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    descriptorWrite.descriptorCount = 1;
    descriptorWrite.pImageInfo = &imageInfo;

    // Update new descriptor set
    vkUpdateDescriptorSets(mDevice.logicalDevice, 1, &descriptorWrite, 0, nullptr);

    // Add descriptor set to list
    mSamplerDescriptorSets.push_back(descriptorSet);

    // Return descriptor set location
    return mSamplerDescriptorSets.size() - 1;
}

stbi_uc* Descriptors::loadTextureFile(const string& fileName, int* width, int* height, VkDeviceSize* imageSize)
{
    // Number of channels image uses
    int channels;

    // Load pixel data for image
    //std::string fileLoc = "Textures/" + fileName;
    stbi_uc* image = stbi_load(fileName.c_str(), width, height, &channels, STBI_rgb_alpha);
    THROW_IF(!image, "Failed to load a Texture file! (" + fileName + ")");

    // Calculate image size using given and known data
    *imageSize = *width * *height * 4;

    return image;
}
