
#include "../common/includes.h" // precompiled header first

#include "../common/Utilities.h"

#include "Buffers.h"

using namespace std;

Buffers::Buffers(GLFWwindow* newWindow) : GraphicsPipelines(newWindow)
{
    createColorBufferImage();
    createDepthBufferImage();
    createFramebuffers();
    createCommandPool();
    createCommandBuffers();
    createUniformBuffers();
}

Buffers::~Buffers()
{
    for (size_t i = 0; i < mSwapChainImages.size(); i++)
    {
        vkDestroyBuffer(mDevice.logicalDevice, mVpUniformBuffer[i], nullptr);
        vkFreeMemory(mDevice.logicalDevice, mVpUniformBufferMemory[i], nullptr);
        //vkDestroyBuffer(mainDevice.logicalDevice, modelDUniformBuffer[i], nullptr);
        //vkFreeMemory(mainDevice.logicalDevice, modelDUniformBufferMemory[i], nullptr);
    }

    vkDestroyCommandPool(mDevice.logicalDevice, mGraphicsCommandPool, nullptr);

    for (size_t i = 0; i < mDepthBuffers.size(); i++)
    {
        vkDestroyImageView(mDevice.logicalDevice, mDepthBuffers[i].imageView, nullptr);
        vkDestroyImage(mDevice.logicalDevice, mDepthBuffers[i].image, nullptr);
        vkFreeMemory(mDevice.logicalDevice, mDepthBuffers[i].imageMemory, nullptr);
    }

    for (size_t i = 0; i < mColorBuffers.size(); i++)
    {
        vkDestroyImageView(mDevice.logicalDevice, mColorBuffers[i].imageView, nullptr);
        vkDestroyImage(mDevice.logicalDevice, mColorBuffers[i].image, nullptr);
        vkFreeMemory(mDevice.logicalDevice, mColorBuffers[i].imageMemory, nullptr);
    }

    for (auto& framebuffer : mSwapChainFramebuffers)
    {
        vkDestroyFramebuffer(mDevice.logicalDevice, framebuffer, nullptr);
    }
}

void Buffers::createColorBufferImage()
{
    mColorBuffers.resize(mSwapChainImages.size());

    for (size_t i = 0; i < mSwapChainImages.size(); i++)
    {
        mColorBuffers[i].image = createImage(mSwapChainExtent.width, mSwapChainExtent.height, mColorAttachmentFormat, VK_IMAGE_TILING_OPTIMAL,
            VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
            &mColorBuffers[i].imageMemory);

        mColorBuffers[i].imageView = createImageView(mColorBuffers[i].image, mColorAttachmentFormat, VK_IMAGE_ASPECT_COLOR_BIT);
    }
}

void Buffers::createDepthBufferImage()
{
    mDepthBuffers.resize(mSwapChainImages.size());

    for (size_t i = 0; i < mSwapChainImages.size(); i++)
    {
        // Create Depth Buffer Image
        mDepthBuffers[i].image = createImage(mSwapChainExtent.width, mSwapChainExtent.height, mDepthAttachmentFormat, VK_IMAGE_TILING_OPTIMAL,
            VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT,
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &mDepthBuffers[i].imageMemory);

        // Create Depth Buffer Image View
        mDepthBuffers[i].imageView = createImageView(mDepthBuffers[i].image, mDepthAttachmentFormat, VK_IMAGE_ASPECT_DEPTH_BIT);
    }
}

void Buffers::createFramebuffers()
{
    // Resize framebuffer count to equal swap chain image count
    mSwapChainFramebuffers.resize(mSwapChainImages.size());

    // Create a framebuffer for each swap chain image
    for (size_t i = 0; i < mSwapChainFramebuffers.size(); i++)
    {
        array<VkImageView, 3> attachments = {
            mSwapChainImages[i].imageView,
            mColorBuffers[i].imageView,
            mDepthBuffers[i].imageView
        };

        VkFramebufferCreateInfo framebufferCreateInfo = {};
        framebufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebufferCreateInfo.renderPass = mRenderPass;                                      // Render Pass layout the Framebuffer will be used with
        framebufferCreateInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
        framebufferCreateInfo.pAttachments = attachments.data();                            // List of attachments (1:1 with Render Pass)
        framebufferCreateInfo.width = mSwapChainExtent.width;                                // Framebuffer width
        framebufferCreateInfo.height = mSwapChainExtent.height;                              // Framebuffer height
        framebufferCreateInfo.layers = 1;                                                   // Framebuffer layers

        VkResult result = vkCreateFramebuffer(mDevice.logicalDevice, &framebufferCreateInfo, nullptr, &mSwapChainFramebuffers[i]);
        THROW_IF(result != VK_SUCCESS, "Failed to create a Framebuffer!");
    }
}

VkImage Buffers::createImage(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags useFlags, VkMemoryPropertyFlags propFlags, VkDeviceMemory* imageMemory)
{
    // CREATE IMAGE
    // Image Creation Info
    VkImageCreateInfo imageCreateInfo = {};
    imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;                       // Type of image (1D, 2D, or 3D)
    imageCreateInfo.extent.width = width;                               // Width of image extent
    imageCreateInfo.extent.height = height;                             // Height of image extent
    imageCreateInfo.extent.depth = 1;                                   // Depth of image (just 1, no 3D aspect)
    imageCreateInfo.mipLevels = 1;                                      // Number of mipmap levels
    imageCreateInfo.arrayLayers = 1;                                    // Number of levels in image array
    imageCreateInfo.format = format;                                    // Format type of image
    imageCreateInfo.tiling = tiling;                                    // How image data should be "tiled" (arranged for optimal reading)
    imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;          // Layout of image data on creation
    imageCreateInfo.usage = useFlags;                                   // Bit flags defining what image will be used for
    imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;                    // Number of samples for multi-sampling
    imageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;            // Whether image can be shared between queues

    // Create image
    VkImage image;
    VkResult result = vkCreateImage(mDevice.logicalDevice, &imageCreateInfo, nullptr, &image);
    THROW_IF(result != VK_SUCCESS, "Failed to create an Image!");

    // CREATE MEMORY FOR IMAGE

    // Get memory requirements for a type of image
    VkMemoryRequirements memoryRequirements;
    vkGetImageMemoryRequirements(mDevice.logicalDevice, image, &memoryRequirements);

    // Allocate memory using image requirements and user defined properties
    VkMemoryAllocateInfo memoryAllocInfo = {};
    memoryAllocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    memoryAllocInfo.allocationSize = memoryRequirements.size;
    memoryAllocInfo.memoryTypeIndex = findMemoryTypeIndex(mDevice.physicalDevice, memoryRequirements.memoryTypeBits, propFlags);

    result = vkAllocateMemory(mDevice.logicalDevice, &memoryAllocInfo, nullptr, imageMemory);
    THROW_IF(result != VK_SUCCESS, "Failed to allocate memory for image!");

    // Connect memory to image
    vkBindImageMemory(mDevice.logicalDevice, image, *imageMemory, 0);

    return image;
}

void Buffers::createCommandPool()
{
    // Get indices of queue families from device
    QueueFamilyIndices queueFamilyIndices = getQueueFamilies(mDevice.physicalDevice);

    VkCommandPoolCreateInfo poolInfo = {};
    poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily;  // Queue Family type that buffers from this command pool will use

    // Create a Graphics Queue Family Command Pool
    VkResult result = vkCreateCommandPool(mDevice.logicalDevice, &poolInfo, nullptr, &mGraphicsCommandPool);
    THROW_IF(result != VK_SUCCESS, "Failed to create a Command Pool!");
}

void Buffers::createCommandBuffers()
{
    // Resize command buffer count to have one for each framebuffer
    mCommandBuffers.resize(mSwapChainFramebuffers.size());

    VkCommandBufferAllocateInfo cbAllocInfo = {};
    cbAllocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    cbAllocInfo.commandPool = mGraphicsCommandPool;
    cbAllocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;    // VK_COMMAND_BUFFER_LEVEL_PRIMARY  : Buffer you submit directly to queue. Cant be called by other buffers.
                                                            // VK_COMMAND_BUFFER_LEVEL_SECONARY : Buffer can't be called directly. Can be called from other buffers via "vkCmdExecuteCommands" when recording commands in primary buffer
    cbAllocInfo.commandBufferCount = static_cast<uint32_t>(mCommandBuffers.size());

    // Allocate command buffers and place handles in array of buffers
    VkResult result = vkAllocateCommandBuffers(mDevice.logicalDevice, &cbAllocInfo, mCommandBuffers.data());
    THROW_IF(result != VK_SUCCESS, "Failed to allocate Command Buffers!");
}

void Buffers::createUniformBuffers()
{
    // ViewProjection buffer size
    VkDeviceSize vpBufferSize = sizeof(UboViewProjection);

    // Model buffer size
    //VkDeviceSize modelBufferSize = modelUniformAlignment * MAX_OBJECTS;

    // One uniform buffer for each image (and by extension, command buffer)
    mVpUniformBuffer.resize(mSwapChainImages.size());
    mVpUniformBufferMemory.resize(mSwapChainImages.size());
    //modelDUniformBuffer.resize(swapChainImages.size());
    //modelDUniformBufferMemory.resize(swapChainImages.size());

    // Create Uniform buffers
    for (size_t i = 0; i < mSwapChainImages.size(); i++)
    {
        createBuffer(mDevice.physicalDevice, mDevice.logicalDevice, vpBufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, &mVpUniformBuffer[i], &mVpUniformBufferMemory[i]);

        /*createBuffer(mainDevice.physicalDevice, mainDevice.logicalDevice, modelBufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, &modelDUniformBuffer[i], &modelDUniformBufferMemory[i]);*/
    }
}

void Buffers::updateUniformBuffers(uint32_t imageIndex)
{
    // Copy VP data
    void* data;
    vkMapMemory(mDevice.logicalDevice, mVpUniformBufferMemory[imageIndex], 0, sizeof(UboViewProjection), 0, &data);
    memcpy(data, &uboViewProjection, sizeof(UboViewProjection));
    vkUnmapMemory(mDevice.logicalDevice, mVpUniformBufferMemory[imageIndex]);

    // Copy Model data
    /*for (size_t i = 0; i < meshList.size(); i++)
    {
        UboModel * thisModel = (UboModel *)((uint64_t)modelTransferSpace + (i * modelUniformAlignment));
        *thisModel = meshList[i].getModel();
    }

    // Map the list of model data
    vkMapMemory(mainDevice.logicalDevice, modelDUniformBufferMemory[imageIndex], 0, modelUniformAlignment * meshList.size(), 0, &data);
    memcpy(data, modelTransferSpace, modelUniformAlignment * meshList.size());
    vkUnmapMemory(mainDevice.logicalDevice, modelDUniformBufferMemory[imageIndex]);*/
}
