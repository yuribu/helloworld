#pragma once

#include "GraphicsPipelines.h"

class Buffers : public GraphicsPipelines
{
public:
    Buffers(GLFWwindow* newWindow);
    ~Buffers();

protected:
    struct ImageBuffer
    {
        VkDeviceMemory imageMemory;
        VkImage image;
        VkImageView imageView;
    };

    std::vector<ImageBuffer> mColorBuffers;
    std::vector<ImageBuffer> mDepthBuffers;

    std::vector<VkFramebuffer> mSwapChainFramebuffers;

    VkCommandPool mGraphicsCommandPool;
    std::vector<VkCommandBuffer> mCommandBuffers;

    // Scene Settings
    struct UboViewProjection {
        glm::mat4 projection;
        glm::mat4 view;
    } uboViewProjection;

    std::vector<VkBuffer> mVpUniformBuffer;
    std::vector<VkDeviceMemory> mVpUniformBufferMemory;

    //std::vector<VkImage> textureImages;
    //std::vector<VkDeviceMemory> textureImageMemory;
    //std::vector<VkImageView> textureImageViews;

    VkImage createImage(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags useFlags,
        VkMemoryPropertyFlags propFlags, VkDeviceMemory* imageMemory);

    void updateUniformBuffers(uint32_t imageIndex);

private:
    void createColorBufferImage();
    void createDepthBufferImage();
    void createFramebuffers();
    void createCommandPool();
    void createCommandBuffers();
    void createUniformBuffers();
};