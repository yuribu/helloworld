#pragma once

#include "Device.h"

class SwapChain : public Device
{
public:
    SwapChain(GLFWwindow* newWindow);
    ~SwapChain();

protected:
    struct SwapchainImage {
        VkImage image;
        VkImageView imageView;
    };

    std::vector<SwapchainImage> mSwapChainImages;

    VkSwapchainKHR mSwapchain;

    // - Utility
    VkFormat mSwapChainImageFormat;
    VkExtent2D mSwapChainExtent;

    VkImageView createImageView(VkImage image, VkFormat format, VkImageAspectFlags aspectFlags);

private:
    VkSurfaceFormatKHR chooseBestSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& formats);
    VkPresentModeKHR chooseBestPresentationMode(const std::vector<VkPresentModeKHR>& presentationModes);
    VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& surfaceCapabilities);
};