#pragma once

#include <tinyxml2.h>

#define FOR_EACH_CHILD(parent, child) \
    for (auto child = parent->FirstChildElement(#child); \
        child != nullptr; child = child->NextSiblingElement(#child))

static const tinyxml2::XMLElement* findChildWithAttribute(const tinyxml2::XMLElement * element,
    const std::string & childName, const std::string & attrKey, const std::string & attrValue)
{
    for (auto child = element->FirstChildElement(childName.c_str()); child != nullptr;
        child = child->NextSiblingElement(childName.c_str()))
    {
        if (child->Attribute(attrKey.c_str(), attrValue.c_str()))
        {
            return child;
        }
    }

    return nullptr;
}

static glm::vec3 parseVec3(const char* text)
{
    std::istringstream iss(text);
    iss.exceptions(std::ios::failbit);

    glm::vec3 vec;
    iss >> vec.x;
    iss >> vec.y;
    iss >> vec.z;

    return vec;
}

static glm::vec4 parseVec4(const char* text)
{
    std::istringstream iss(text);
    iss.exceptions(std::ios::failbit);

    glm::vec4 vec;
    iss >> vec.x;
    iss >> vec.y;
    iss >> vec.z;
    iss >> vec.w;

    return vec;
}

static glm::mat4 parseMat4(const char* text)
{
    std::istringstream iss(text);
    iss.exceptions(std::ios::failbit);

    glm::mat4 mat;
    for (size_t y = 0; y < 4; y++)
    {
        for (size_t x = 0; x < 4; x++)
        {
            iss >> mat[y][x];
        }
    }

    return mat;
}