#pragma once

#include "../common/ModelTree.h"

ModelTree processGeometry(const tinyxml2::XMLElement* root, const std::string& geometryId);
