
#include "../common/includes.h" // precompiled header first

#include <assert.h>

#include "DaeUtilites.h"
#include "DaeMaterial.h"

using namespace tinyxml2;
using namespace std;

void DaeMaterial::findMaterial(const XMLElement* root, const string& symbol, const string& target)
{
    auto library_materials = root->FirstChildElement("library_materials");
    assert(library_materials);

    auto material = findChildWithAttribute(library_materials, "material", "id", target.c_str());
    assert(material);

    auto instance_effect = material->FirstChildElement("instance_effect");
    assert(instance_effect);

    string url = instance_effect->Attribute("url");
    assert(!url.empty());
    url = url.substr(1);

    auto library_effects = root->FirstChildElement("library_effects");
    assert(library_effects);

    auto effect = findChildWithAttribute(library_effects, "effect", "id", url);
    assert(effect);

    auto profile_COMMON = effect->FirstChildElement("profile_COMMON");
    assert(profile_COMMON);

    auto technique = profile_COMMON->FirstChildElement("technique");
    assert(technique);

    auto blinn = technique->FirstChildElement("blinn");
    assert(blinn);

    auto diffuse = blinn->FirstChildElement("diffuse");
    assert(diffuse);

    auto color = diffuse->FirstChildElement("color");
    if (color)
    {
        istringstream iss(color->GetText());
        iss.exceptions(ios::failbit);

        glm::vec4 col;

        iss >> col.r;
        iss >> col.g;
        iss >> col.b;
        iss >> col.a;

        mTextureColors[symbol] = col;
    }
    else
    {
        auto texture = diffuse->FirstChildElement("texture");
        assert(texture);

        string textureId = texture->Attribute("texture");
        assert(!textureId.empty());

        assert(texture->Attribute("texcoord", "UVSET0"));

        auto newparam = findChildWithAttribute(profile_COMMON, "newparam", "sid", textureId.c_str());
        assert(newparam);

        auto sampler2D = newparam->FirstChildElement("sampler2D");
        assert(sampler2D);

        auto instance_image = sampler2D->FirstChildElement("instance_image");
        assert(instance_image);

        string imageId = instance_image->Attribute("url");
        assert(!imageId.empty());
        imageId = imageId.substr(1);

        auto library_images = root->FirstChildElement("library_images");
        assert(library_images);

        auto image = findChildWithAttribute(library_images, "image", "id", imageId);
        assert(image);

        auto init_from = image->FirstChildElement("init_from");
        assert(init_from);

        auto ref = init_from->FirstChildElement("ref");
        assert(ref);

        string imageFile = ref->GetText();
        assert(!imageFile.empty());

        mTextureFiles[symbol] = imageFile;
    }
}

void DaeMaterial::processMaterials(const XMLElement* root, const XMLElement* bind_material)
{
    auto technique_common = bind_material->FirstChildElement("technique_common");
    assert(technique_common);

    // find targets
    FOR_EACH_CHILD(technique_common, instance_material)
    {
        string symbol = instance_material->Attribute("symbol");
        assert(!symbol.empty());

        string target = instance_material->Attribute("target");
        assert(!target.empty());
        target = target.substr(1);

        // check
        auto bind_vertex_input = instance_material->FirstChildElement("bind_vertex_input");
        assert(bind_vertex_input);
        assert(bind_vertex_input->Attribute("semantic", "UVSET0"));
        assert(bind_vertex_input->Attribute("input_semantic", "TEXCOORD"));
        assert(bind_vertex_input->Attribute("input_set", "0"));

        findMaterial(root, symbol, target);
    }
}
