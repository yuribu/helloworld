#pragma once

#include "../common/Utilities.h"
#include "../common/ModelTree.h"

#include "DaeMaterial.h"

class DaeParser : public DaeMaterial
{
public:
    void parse(const std::string& daeFile, std::vector<ModelTree>& modelList);

private:
    void processVisualScene(const tinyxml2::XMLElement* root, std::vector<ModelTree>& modelList);
    ModelTree processNode(const tinyxml2::XMLElement* node, const tinyxml2::XMLElement* root);

};