
#include "../common/includes.h" // precompiled header first

#include <tinyxml2.h>
#include <assert.h>

#include "DaeUtilites.h"
#include "DaeGeometry.h"
#include "DaeParser.h"

using namespace tinyxml2;
using namespace std;

void DaeParser::parse(const string& daeFile, std::vector<ModelTree>& modelList)
{
    XMLDocument doc;

    XMLError error = doc.LoadFile(daeFile.c_str());
    if (error != XML_SUCCESS)
    {
        throw runtime_error("Failed to load dae file: " + daeFile);
    }

    XMLElement* root = doc.RootElement();
    assert(root);

    processVisualScene(root, modelList);
}


void DaeParser::processVisualScene(const tinyxml2::XMLElement* root, std::vector<ModelTree>& modelList)
{
    auto library_visual_scenes = root->FirstChildElement("library_visual_scenes");
    assert(library_visual_scenes);

    auto visual_scene = library_visual_scenes->FirstChildElement("visual_scene");
    assert(visual_scene);
    // expected once
    assert(visual_scene->NextSiblingElement("visual_scene") == nullptr);

    FOR_EACH_CHILD(visual_scene, node)
    {
        modelList.emplace_back(processNode(node, root));
    }
}

ModelTree DaeParser::processNode(const tinyxml2::XMLElement* node, const tinyxml2::XMLElement* root)
{
    ModelTree modelTree;
    glm::mat4 model(1.0f);

    // model matrix transform
    for (auto transform = node->FirstChildElement(); transform != nullptr;
        transform = transform->NextSiblingElement())
    {
        string operation(transform->Name());

        if ("translate" == operation)
        {
            glm::vec3 translate = parseVec3(transform->GetText());

            model = glm::translate(model, translate);
        }
        else if ("rotate" == operation)
        {
            glm::vec4 rotate = parseVec4(transform->GetText());

            rotate.w = glm::radians(rotate.w);

            model = glm::rotate(model, rotate.w, glm::vec3(rotate));
        }
        else if ("scale" == operation)
        {
            glm::vec3 scale = parseVec3(transform->GetText());

            model = glm::scale(model, scale);
        }
        else if ("transform" == operation)
        {
            glm::mat4 transformMat = parseMat4(transform->GetText());

            model = transformMat * model;
        }
    }

    auto instance_geometry = node->FirstChildElement("instance_geometry");
    if (instance_geometry)
    {
        string geometryId = instance_geometry->Attribute("url");
        assert(!geometryId.empty());
        geometryId = geometryId.substr(1);

        auto bind_material = instance_geometry->FirstChildElement("bind_material");
        if (bind_material)
        {
            modelTree = processGeometry(root, geometryId);

            processMaterials(root, bind_material);
        }
    }

    for (auto child = node->FirstChildElement("node"); child != nullptr;
        child = child->NextSiblingElement("node"))
    {
        modelTree.addChild(processNode(child, root));
    }

    modelTree.applyTransform(model);

    return modelTree;
}

