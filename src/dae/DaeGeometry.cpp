
#include "../common/includes.h" // precompiled header first

#include <tinyxml2.h>
#include <assert.h>

#include "DaeUtilites.h"
#include "DaeGeometry.h"

using namespace tinyxml2;
using namespace std;

struct DaeSource
{
    vector<float> float_array;
    int stride;
    string paramNames;
};

ModelTree processGeometry(const XMLElement* root, const string& geometryId)
{
    auto library_geometries = root->FirstChildElement("library_geometries");
    assert(library_geometries);

    // find by geometryId
    auto geometry = library_geometries->FirstChildElement("geometry");
    for (; geometry != nullptr; geometry = geometry->NextSiblingElement("geometry"))
    {
        if (geometry->Attribute("id", geometryId.c_str()))
        {
            break;
        }
    }

    assert(geometry);

    auto mesh = geometry->FirstChildElement("mesh");
    assert(mesh);

    map<string, DaeSource> sourcesMap;

    // parse sources
    FOR_EACH_CHILD(mesh, source)
    {
        DaeSource daeSource;

        auto techique_common = source->FirstChildElement("technique_common");
        assert(techique_common);

        auto accessor = techique_common->FirstChildElement("accessor");
        assert(accessor);

        int count;
        auto result = accessor->QueryIntAttribute("count", &count);
        assert(result == XML_SUCCESS);

        string arrayId = accessor->Attribute("source");
        assert(!arrayId.empty());
        arrayId = arrayId.substr(1);

        result = accessor->QueryIntAttribute("stride", &daeSource.stride);
        assert(result == XML_SUCCESS);

        for (auto param = accessor->FirstChildElement("param"); param != nullptr;
            param = param->NextSiblingElement("param"))
        {
            auto name = param->Attribute("name");
            daeSource.paramNames += name;

            auto type = param->Attribute("type");
            assert(string(type) == "float");
        }

        auto float_array = source->FirstChildElement("float_array");
        assert(float_array);
        assert(float_array->Attribute("id", arrayId.c_str()));

        int arrayCount;
        result = float_array->QueryIntAttribute("count", &arrayCount);
        assert(result == XML_SUCCESS);
        assert(arrayCount == count * daeSource.stride);

        daeSource.float_array.resize(arrayCount);

        istringstream iss(float_array->GetText());
        iss.exceptions(ios::failbit);

        for (size_t i = 0; i < daeSource.float_array.size(); i++)
        {
            iss >> daeSource.float_array[i];
        }

        string sourceId = source->Attribute("id");
        assert(!sourceId.empty());

        sourcesMap.emplace(sourceId, daeSource);
    }

    ModelTree modelTree;

    FOR_EACH_CHILD(mesh, triangles)
    {
        int offsetVert = -1;
        int offsetTex = -1;
        int maxOffset = -1;

        string sourceVert;
        string sourceTex;

        // read triangles input
        FOR_EACH_CHILD(triangles, input)
        {
            int set = -1;
            auto result = input->QueryIntAttribute("set", &set);
            assert(result == XML_SUCCESS && set == 0);

            int tmpOffset;
            result = input->QueryIntAttribute("offset", &tmpOffset);
            assert(result == XML_SUCCESS);

            if (input->Attribute("semantic", "VERTEX"))
            {
                sourceVert = input->Attribute("source");
                assert(!sourceVert.empty());
                sourceVert = sourceVert.substr(1);

                offsetVert = tmpOffset;
            }
            else if (input->Attribute("semantic", "TEXCOORD"))
            {
                sourceTex = input->Attribute("source");
                assert(!sourceTex.empty());
                sourceTex = sourceTex.substr(1);

                offsetTex = tmpOffset;
            }

            if (maxOffset < tmpOffset)
            {
                maxOffset = tmpOffset;
            }
        }

        assert(offsetVert >= 0 && offsetTex >= 0);

        // read vertices
        auto vertices = mesh->FirstChildElement("vertices");
        assert(vertices);

        assert(vertices->Attribute("id", sourceVert.c_str()));

        auto input = vertices->FirstChildElement("input");
        assert(input);

        // update sourceVert
        assert(input->Attribute("semantic", "POSITION"));
        sourceVert = input->Attribute("source");
        sourceVert = sourceVert.substr(1);

        auto findVert = sourcesMap.find(sourceVert);
        assert(findVert != sourcesMap.end());
        assert(findVert->second.paramNames == "XYZ");
        assert(findVert->second.stride == 3);

        auto findTex = sourcesMap.find(sourceTex);
        assert(findTex->second.paramNames == "ST");
        assert(findTex->second.stride == 2);

        int trianglesCount;
        auto result = triangles->QueryIntAttribute("count", &trianglesCount);
        assert(result == XML_SUCCESS);

        string material = triangles->Attribute("material");
        assert(!material.empty());

        Mesh newMesh(trianglesCount * 2, trianglesCount * 3); // approximately

        newMesh.setMaterial(material);

        // parse, set vertices, texCoords and it indices
        auto p = triangles->FirstChildElement("p");
        assert(p);

        istringstream iss(p->GetText());
        iss.exceptions(ios::failbit);

        set<Vertex> vertexSet;

        for (size_t i = 0; i < (trianglesCount * 3); i++)
        {
            Vertex vertex;

            for (int o = 0; o <= maxOffset; o++)
            {
                uint32_t index;
                iss >> index;

                if (o == offsetVert)
                {
                    vertex.pos.x = findVert->second.float_array[index * 3];
                    vertex.pos.y = findVert->second.float_array[index * 3 + 1];
                    vertex.pos.z = findVert->second.float_array[index * 3 + 2];
                }
                else if (o == offsetTex)
                {
                    vertex.tex.s = findTex->second.float_array[index * 2];
                    vertex.tex.t = findTex->second.float_array[index * 2 + 1];
                    // flip to uv
                    vertex.tex.t = 1.0f - vertex.tex.t;
                }
            }

            if (vertexSet.insert(vertex).second)
            {
                size_t count = newMesh.addVertex(vertex);
                newMesh.addIndex(count - 1);
            }
            else // already exist
            {
                size_t index = newMesh.findVetrexIndex(vertex);
                assert(index >= 0);

                newMesh.addIndex(index);
            }
        }

        modelTree.addMesh(newMesh);
    }

    assert(modelTree.getMeshCount() > 0);

    return modelTree;
}
