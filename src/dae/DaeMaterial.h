#pragma once

#include <tinyxml2.h>

class DaeMaterial
{
public:
    const std::map<std::string, std::string>& getTextureFiles() { return mTextureFiles; }
    const std::map<std::string, glm::vec4>& getTextureColors() { return mTextureColors; }

protected:
    void processMaterials(const tinyxml2::XMLElement* root, const tinyxml2::XMLElement* bind_material);
    void findMaterial(const tinyxml2::XMLElement* root, const std::string& symbol, const std::string& target);

private:
    std::map<std::string, std::string> mTextureFiles;
    std::map<std::string, glm::vec4> mTextureColors;
};